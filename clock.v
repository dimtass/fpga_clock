module Clock(
	input clk_1hz,
	input [3:0]keys,		// Buttons to change the time
								//		b0: +1 minute
								//		b1: -1 minute
								//		b2: +1 hour
								//		b3: -1 hour
	output [5:0]seconds,	// 0-59 seconds
	output [5:0]minutes,	// 0-59 minutes
	output [5:0]hours		// 0-24 hours
);

wire minute_tick = (seconds == 6'd60);	// set when seconds=60
wire hour_tick = (minutes == 6'd60);	// set when minutes=60
wire day_tick = (hours == 6'd24);		// set when hours=60

reg [5:0]seconds_reg;
reg [5:0]minutes_reg;
reg [5:0]hours_reg;

/* increment seconds */
always@(posedge clk_1hz)
begin
	/* 1-sec event */
	if (clk_1hz) begin
		seconds_reg <= seconds_reg + 6'd1;
		
		/* 1 minute passed? */
		if (seconds_reg == 6'd59) begin
			seconds_reg <= 6'd0; 					// reset seconds
			minutes_reg <= minutes_reg + 6'd1;	// increment minutes
			
			/* 1 hour passed? */
			if (minutes_reg == 6'd59) begin
				hours_reg <= hours_reg + 6'd1;	// increment hours
				minutes_reg <= 6'd0;					// reset minutes
				
				/* 1 day passed? */
				if (hours_reg == 6'd23) begin
					hours_reg <= 6'd0;				// reset hours
				end
			end
		end
	end
	
	/* increment minutes */
	if (keys[1] == 1'b1) minutes_reg <= (minutes_reg < 6'd59)? minutes_reg + 6'd1 : 6'd0;
	/* decrement minutes */
	if (keys[0] == 1'b1) minutes_reg <= (minutes_reg > 6'd0)? minutes_reg - 6'd1 : 6'd59;
	/* increment hours */
	if (keys[3] == 1'b1) hours_reg <= (hours_reg < 6'd23)? hours_reg + 6'd1 : 6'd0;
	/* decrement hours */
	if (keys[2] == 1'b1) hours_reg <= (hours_reg > 6'd0)? hours_reg - 6'd1 : 6'd23;
end


assign seconds = seconds_reg;
assign minutes = minutes_reg;
assign hours = hours_reg;

endmodule
