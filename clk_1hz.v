module Clocks(
	input clk,
	output clk_1hz,
	output clk_2hz,
	output clk_5hz
);

	parameter MAIN_CLK_HZ=32'd48_000_000;
	
	parameter CLK_1HZ=0;
	parameter CLK_2HZ=1;
	parameter CLK_5HZ=2;
	
	reg [31:0]cnt[2:0];
	reg flag_hz[2:0];
	
	// 1-Hz timer
	always@(posedge clk)
		if(cnt[CLK_1HZ] == MAIN_CLK_HZ/2)
//		if(cnt[0] == MAIN_CLK_HZ/360)
			begin 
				cnt[CLK_1HZ] <= 32'b0; 
				flag_hz[CLK_1HZ] <= !flag_hz[CLK_1HZ];
			end
		else cnt[CLK_1HZ] <= cnt[CLK_1HZ] + 1'b1;
		
	assign clk_1hz = flag_hz[CLK_1HZ];
		
	// 2-Hz timer
	always@(posedge clk)
		if(cnt[CLK_2HZ] == MAIN_CLK_HZ/4)
			begin 
				cnt[CLK_2HZ] <= 32'b0; 
				flag_hz[CLK_2HZ] <= !flag_hz[CLK_2HZ];
			end
		else cnt[CLK_2HZ] <= cnt[CLK_2HZ] + 1'b1;
		
	assign clk_2hz = flag_hz[CLK_2HZ];
		
	// 5-Hz timer
	always@(posedge clk)
		if(cnt[CLK_5HZ] == MAIN_CLK_HZ/(2*5))
			begin 
				cnt[CLK_5HZ] <= 32'b0; 
				flag_hz[CLK_5HZ] <= !flag_hz[CLK_5HZ];
			end
		else cnt[CLK_5HZ] <= cnt[1] + 1'b1;
		
		assign clk_5hz = flag_hz[CLK_5HZ];
	

endmodule
