module Buzzer(
	input clk,		// main clock input
	input sound,
	output buzzer_out
);

	parameter FREQ=32'd48_000;

	reg [31:0]cnt;
	reg clk_hz;

	always@(posedge clk)
		if (cnt == FREQ/2)
			begin
				cnt <= 32'b0;
				clk_hz = !clk_hz;
			end
		else
			cnt <= cnt + 1'b1;
	
	assign buzzer_out = sound ? clk_hz : 1'bz;

endmodule
