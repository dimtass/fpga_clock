/**
* function: 7-segment HEX driver for 4x 7-segment displays on the dev board
*
* Sub-module: none
*
* Version: V0.00
*
* Date: 6/9/2016
*
**
*  Set each 7-segment's refresh rate to at least 25 fps. To do that divide the
*	high frequency input clk by 4*25
*
*			  (0)			   	  (a)
*		   -------           -------
*		   |		|           |		|
*		(5)| (6)	|(1)     (f)| (g)	|(b)
*		   -------           -------
*		  	|		|          	|		|
*		(4)|		|(2)     (e)|		|(c)
*		   ------- o(7)      ------- o (p)
*			  (3)			   	  (d)
*
*	Segment bits/pins:
*	0: DS_A
*	1:	DS_B
*	2:	DS_C
*	3: DS_D
*	4: DS_E
*	5: DS_F
*	6: DS_G
*	7: DS_P (is not used here)
*/

module SegDisplay(
	input	 clk,				// clock input (48MHz)
	input  [3:0]disp1,	// value of the first 7-seg display on the right
	input  [3:0]disp2,	// value of the 2nd display
	input  [3:0]disp3,	// value of the 3rd display
	input  [3:0]disp4,	// value of the 4th display
	output reg [6:0]seg_pins,	// Segment pins (see above note)
	output reg [3:0]disp_en		// Enable/disable display
										// b0: Enable display 1
										// b1: Enable display 2
										// b2: Enable display 3
										// b4: Enable display 4
);



parameter [6:0]DIGIT_0=7'b0111111;
parameter [6:0]DIGIT_1=7'b0000110;
parameter [6:0]DIGIT_2=7'b1011011;
parameter [6:0]DIGIT_3=7'b1001111;
parameter [6:0]DIGIT_4=7'b1100110;
parameter [6:0]DIGIT_5=7'b1101101;
parameter [6:0]DIGIT_6=7'b1111101;
parameter [6:0]DIGIT_7=7'b0000111;
parameter [6:0]DIGIT_8=7'b1111111;
parameter [6:0]DIGIT_9=7'b1101111;
parameter [6:0]DIGIT_A=7'b1110111;
parameter [6:0]DIGIT_B=7'b1111100;
parameter [6:0]DIGIT_C=7'b1011000;
parameter [6:0]DIGIT_D=7'b1011110;
parameter [6:0]DIGIT_E=7'b1111001;
parameter [6:0]DIGIT_F=7'b1110001;
parameter [6:0]DIGIT_EMPTY=7'b0000000;

parameter [3:0]EN_DISP1=4'b1110;
parameter [3:0]EN_DISP2=4'b1101;
parameter [3:0]EN_DISP3=4'b1011;
parameter [3:0]EN_DISP4=4'b0111;
parameter [3:0]EN_DISP_ALL=4'b0000;

reg [31:0]cnt;
always @(posedge clk)
	begin
		cnt <= cnt + 1'b1;
	end

always @(posedge clk)
	begin
		if(cnt[16:15] == 2'b00)
			case (disp1)
			4'h0 :   begin seg_pins = DIGIT_0; disp_en=EN_DISP1;end
			4'h1 :   begin seg_pins = DIGIT_1; disp_en=EN_DISP1;end
			4'h2 :   begin seg_pins = DIGIT_2; disp_en=EN_DISP1;end
			4'h3 :   begin seg_pins = DIGIT_3; disp_en=EN_DISP1;end
			4'h4 :   begin seg_pins = DIGIT_4; disp_en=EN_DISP1;end
			4'h5 :   begin seg_pins = DIGIT_5; disp_en=EN_DISP1;end
			4'h6 :   begin seg_pins = DIGIT_6; disp_en=EN_DISP1;end
			4'h7 :   begin seg_pins = DIGIT_7; disp_en=EN_DISP1;end
			4'h8 :   begin seg_pins = DIGIT_8; disp_en=EN_DISP1;end
			4'h9 :   begin seg_pins = DIGIT_9; disp_en=EN_DISP1;end
			4'ha :   begin seg_pins = DIGIT_A; disp_en=EN_DISP1;end
			4'hb :   begin seg_pins = DIGIT_B; disp_en=EN_DISP1;end
			4'hc :   begin seg_pins = DIGIT_C; disp_en=EN_DISP1;end
			4'hd :   begin seg_pins = DIGIT_D; disp_en=EN_DISP1;end
			4'he :   begin seg_pins = DIGIT_E; disp_en=EN_DISP1;end
			4'hf :   begin seg_pins = DIGIT_F; disp_en=EN_DISP1;end
			default: begin seg_pins = DIGIT_EMPTY; disp_en=EN_DISP1;end
			endcase  
		if(cnt[16:15] == 2'b01)
			case (disp2)
			4'h0 :   begin seg_pins = DIGIT_0; disp_en=EN_DISP2;end
			4'h1 :   begin seg_pins = DIGIT_1; disp_en=EN_DISP2;end
			4'h2 :   begin seg_pins = DIGIT_2; disp_en=EN_DISP2;end
			4'h3 :   begin seg_pins = DIGIT_3; disp_en=EN_DISP2;end
			4'h4 :   begin seg_pins = DIGIT_4; disp_en=EN_DISP2;end
			4'h5 :   begin seg_pins = DIGIT_5; disp_en=EN_DISP2;end
			4'h6 :   begin seg_pins = DIGIT_6; disp_en=EN_DISP2;end
			4'h7 :   begin seg_pins = DIGIT_7; disp_en=EN_DISP2;end
			4'h8 :   begin seg_pins = DIGIT_8; disp_en=EN_DISP2;end
			4'h9 :   begin seg_pins = DIGIT_9; disp_en=EN_DISP2;end
			4'ha :   begin seg_pins = DIGIT_A; disp_en=EN_DISP2;end
			4'hb :   begin seg_pins = DIGIT_B; disp_en=EN_DISP2;end
			4'hc :   begin seg_pins = DIGIT_C; disp_en=EN_DISP2;end
			4'hd :   begin seg_pins = DIGIT_D; disp_en=EN_DISP2;end
			4'he :   begin seg_pins = DIGIT_E; disp_en=EN_DISP2;end
			4'hf :   begin seg_pins = DIGIT_F; disp_en=EN_DISP2;end
			default: begin seg_pins = DIGIT_EMPTY; disp_en=EN_DISP2;end
			endcase  
		if(cnt[16:15] == 2'b10)
			case (disp3)
			4'h0 :   begin seg_pins = DIGIT_0; disp_en=EN_DISP3;end
			4'h1 :   begin seg_pins = DIGIT_1; disp_en=EN_DISP3;end
			4'h2 :   begin seg_pins = DIGIT_2; disp_en=EN_DISP3;end
			4'h3 :   begin seg_pins = DIGIT_3; disp_en=EN_DISP3;end
			4'h4 :   begin seg_pins = DIGIT_4; disp_en=EN_DISP3;end
			4'h5 :   begin seg_pins = DIGIT_5; disp_en=EN_DISP3;end
			4'h6 :   begin seg_pins = DIGIT_6; disp_en=EN_DISP3;end
			4'h7 :   begin seg_pins = DIGIT_7; disp_en=EN_DISP3;end
			4'h8 :   begin seg_pins = DIGIT_8; disp_en=EN_DISP3;end
			4'h9 :   begin seg_pins = DIGIT_9; disp_en=EN_DISP3;end
			4'ha :   begin seg_pins = DIGIT_A; disp_en=EN_DISP3;end
			4'hb :   begin seg_pins = DIGIT_B; disp_en=EN_DISP3;end
			4'hc :   begin seg_pins = DIGIT_C; disp_en=EN_DISP3;end
			4'hd :   begin seg_pins = DIGIT_D; disp_en=EN_DISP3;end
			4'he :   begin seg_pins = DIGIT_E; disp_en=EN_DISP3;end
			4'hf :   begin seg_pins = DIGIT_F; disp_en=EN_DISP3;end
			default: begin seg_pins = DIGIT_EMPTY; disp_en=EN_DISP3;end
			endcase  
		if(cnt[16:15] == 2'b11)
			case (disp4)
			4'h0 :   begin seg_pins = DIGIT_0; disp_en=EN_DISP4;end
			4'h1 :   begin seg_pins = DIGIT_1; disp_en=EN_DISP4;end
			4'h2 :   begin seg_pins = DIGIT_2; disp_en=EN_DISP4;end
			4'h3 :   begin seg_pins = DIGIT_3; disp_en=EN_DISP4;end
			4'h4 :   begin seg_pins = DIGIT_4; disp_en=EN_DISP4;end
			4'h5 :   begin seg_pins = DIGIT_5; disp_en=EN_DISP4;end
			4'h6 :   begin seg_pins = DIGIT_6; disp_en=EN_DISP4;end
			4'h7 :   begin seg_pins = DIGIT_7; disp_en=EN_DISP4;end
			4'h8 :   begin seg_pins = DIGIT_8; disp_en=EN_DISP4;end
			4'h9 :   begin seg_pins = DIGIT_9; disp_en=EN_DISP4;end
			4'ha :   begin seg_pins = DIGIT_A; disp_en=EN_DISP4;end
			4'hb :   begin seg_pins = DIGIT_B; disp_en=EN_DISP4;end
			4'hc :   begin seg_pins = DIGIT_C; disp_en=EN_DISP4;end
			4'hd :   begin seg_pins = DIGIT_D; disp_en=EN_DISP4;end
			4'he :   begin seg_pins = DIGIT_E; disp_en=EN_DISP4;end
			4'hf :   begin seg_pins = DIGIT_F; disp_en=EN_DISP4;end
			default: begin seg_pins = DIGIT_EMPTY; disp_en=EN_DISP4;end
			endcase  
	end 

endmodule


