module KeyHandler(
	input clk,
	input [3:0]keys,
	output [3:0]key_press
);

	parameter DIV_FACTOR = 32'd48_000;	//1ms debounce timer

	reg clk_khz;
	reg [31:0]cnt;
	always@(posedge clk)
		if(cnt == DIV_FACTOR/2)
			begin
				cnt <= 32'b0; 
				clk_khz = !clk_khz;
			end
		else cnt <= cnt + 1'b1;

	reg [31:0]key_reg0;
	reg [31:0]key_reg1;
	reg [31:0]key_reg2;
	reg [31:0]key_reg3;
	always@(posedge clk_khz)
		begin
			key_reg0 <= {key_reg0[30:0],!keys[0]};
			key_reg1 <= {key_reg1[30:0],!keys[1]};
			key_reg2 <= {key_reg2[30:0],!keys[2]};
			key_reg3 <= {key_reg3[30:0],!keys[3]};
		end

	assign key_press[0] = (key_reg0 == 32'hffffffff);
	assign key_press[1] = (key_reg1 == 32'hffffffff);
	assign key_press[2] = (key_reg2 == 32'hffffffff);
	assign key_press[3] = (key_reg3 == 32'hffffffff);

endmodule
