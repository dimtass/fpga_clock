module NumSplit(
	input [5:0]number,	// the number to split into digits
	output reg [3:0]digit1,	// most significant digit
	output reg [3:0]digit2	// less significant digit
);

/* Truncate the number and take the differene */
function [3:0]diff(input [5:0]a, input [3:0]b);
	diff = a[3:0]-b;
endfunction

always@(*)
begin
	if (number >= 6'd50) begin
		digit1 = 4'd5;
		digit2 = diff(number, 4'd50);
	end
	else if (number >= 6'd40) begin
		digit1 = 4'd4;
		digit2 = diff(number, 4'd40);
	end
	else if (number >= 6'd30) begin
		digit1 = 4'd3;
		digit2 = diff(number, 4'd30);
	end
	else if (number >= 6'd20) begin
		digit1 = 4'd2;
		digit2 = diff(number, 4'd20);
	end
	else if (number >= 6'd10) begin
		digit1 = 4'd1;
		digit2 = diff(number, 4'd10);
	end
	else begin
		digit1 = 4'd0;
		digit2 = number[3:0];
	end
end

endmodule
