module fpga_clock(
	/* Clock input */
	input CLK,
	/* Buttons */
	input KEY1,KEY2,KEY3,KEY4,
	/* LEDs */
	output DS_C,DS_D,DS_G,DS_DP,
	/* 7-segment display */
	output DS_A,DS_B,DS_E,DS_F,
	output DS_EN1,DS_EN2,DS_EN3,DS_EN4,
	/* Buzzer */
	output BP1
);

	/* 1Hz clock */
	wire clk_1hz;
	wire clk_2hz;
	wire clk_5hz;
	Clocks clocks
	(
		.clk(CLK),
		.clk_1hz(clk_1hz),
		.clk_2hz(clk_2hz),
		.clk_5hz(clk_5hz)
	);
	
	
	/* Button debounce */
	wire [3:0]key = {KEY4,KEY3,KEY2,KEY1};	// assign the key buttons on the keys
	wire [3:0]key_press;
	KeyHandler keyhandler(
		.clk(CLK),
		.keys(key),
		.key_press(key_press)
	);

	
	/* Clock */
	wire [5:0]seconds;
	wire [5:0]minutes;
	wire [5:0]hours;
	Clock clock (
		.clk_1hz(clk_1hz),
		.keys(key_press),
		.seconds(seconds),
		.minutes(minutes),
		.hours(hours)
	);

	
	/* 7-segment display */
	wire [3:0]disp[3:0];	// an array of 4 numbers. These numbers are the HH:MM (MSB->LSB)
	wire [6:0]seg_pins;	// a wire for the 7-segment pin output
	wire [3:0]disp_en;	// the enable bit for each segment
	
	assign {DS_G,DS_F,DS_E,DS_D,DS_C,DS_B,DS_A} = seg_pins ;
	assign {DS_EN1,DS_EN2,DS_EN3,DS_EN4} = disp_en;
	SegDisplay seg(
		.clk(CLK),
		.disp1(disp[0]),
		.disp2(disp[1]),
		.disp3(disp[2]),
		.disp4(disp[3]),
		.seg_pins(seg_pins),
		.disp_en(disp_en)
	);
	/* 2-Hz dot blinking */ 
	reg seg_dot;
	assign DS_DP = seg_dot;
	always@(posedge clk_2hz) seg_dot = !seg_dot;

	
	/* splits minute number to two digits */
	NumSplit min_split(
		.number(minutes),
		.digit1(disp[1]),	// most significant minute number
		.digit2(disp[0])	// less significant minute number
	);

	
	/* Split hours number to two digits */
	NumSplit hour_split(
		.number(hours),
		.digit1(disp[3]),
		.digit2(disp[2])
	);
	
	/* detect any button press */
	wire button_press = key_press[0] | key_press[1] | key_press[2] | key_press[3];
		
	/* buzzer */
	Buzzer buzzer(
		.clk(CLK),
		.sound(button_press),
		.buzzer_out(BP1)
	);

endmodule
